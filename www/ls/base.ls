ig.fit!

$ '#article p:nth-of-type(8)' .after '<div class="infobox-container"><div class="infobox">
  <h3>Kalendář F1 2016</h3>
  <p>
    20. 03. VC Austrálie<br>
    03. 04. VC Bahrajnu<br>
    17. 04. VC Číny<br>
    01. 05. VC Ruska<br>
    15. 05. VC Španělska<br>
    29. 05. VC Monaka<br>
    12. 06. VC Kanady<br>
    19. 06. VC Evropy (Ázerbájdžán)<br>
    03. 07. VC Rakouska<br>
    10. 07. VC Velká Británie<br>
    24. 07. VC Maďarska<br>
    31. 07. VC Německa<br>
    28. 08. VC Belgie<br>
    04. 09. VC Itálie<br>
    18. 09. VC Singapuru<br>
    02. 10. VC Malajsie<br>
    09. 10. VC Japonska<br>
    23. 10. VC USA<br>
    30. 10. VC Mexika<br>
    13. 11. VC Brazílie<br>
    27. 11. VC Abú Zabí
  </p>
</div></div>'

$ '#article p:nth-of-type(35)' .before '<div class="infobox-container"><div class="infobox">
<h3>Startovní listina F1<br>pro sezonu 2016</h3>
<p>
  <b>Mercedes</b><br>
  Nico Rosberg<br>
  Lewis Hamilton
</p>

<p>
  <b>Ferrari</b><br>
  Sebastian Vettel<br>
  Kimi Räikkönen
</p>

<p>
  <b>Red Bull TAG-Heuer</b><br>
  Daniel Ricciardo<br>
  Daniil Kvja
</p>

<p>
  <b>Williams-Mercedes</b><br>
  Valtteri Bottas<br>
  Felipe Mass
</p>

<p>
  <b>McLaren-Honda</b><br>
  Fernando Alonso<br>
  Jenson Button
</p>

<p>
  <b>Force India-Mercedes</b><br>
  Nico Hülkenberg<br>
  Sergio Pérez
</p>

<p>
  <b>Toro Rosso-Ferrari</b><br>
  Max Verstappen<br>
  Carlos Sainz
</p>

<p>
  <b>Renault</b><br>
  Kevin Magnussen<br>
  Jolyon Palme
</p>

<p>
  <b>Sauber-Ferrari</b><br>
  Marcus Ericsson<br>
  Felipe Nasr
</p>

<p>
  <b>Manor-Mercedes</b><br>
  Pascal Wehrlein<br>
  Rio Haryant
</p>

<p>
  <b>Haas-Ferrari</b><br>
  Romain Grosjean<br>
  Esteban Gutiérre
</p>
</div></div>'
